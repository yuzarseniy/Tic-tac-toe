def cout(box) -> None:
  print("●━━━●━━━●━━━●")
  for i in box[:3]:
    print("┃ " + i, end=" ")
  print("┃\n●━━━●━━━●━━━●")
  for i in box[3:6]:
    print("┃ " + i, end=" ")
  print("┃\n●━━━●━━━●━━━●")
  for i in box[6:]:
    print("┃ " + i, end=" ")
  print("┃\n●━━━●━━━●━━━●")

def reading(box, count) -> bool:
  try:
    pos = int(input(("2nd: " if (count % 2) else "1st: "))) - 1
    if (box[pos] == " " and pos >= 0):
      box[pos] = "x" if (count % 2) else "o"
      return True
  except ValueError:
    return False
  except IndexError:
    return False
  else:
    return False

def check(box) -> bool:
  for i in 0, 3, 6:
    if (box[i] == box[i+1] == box[i+2] != " "):
      return True
  for i in 0, 1, 2:
    if (box[i] == box[i+3] == box[i+6] != " "):
      return True
  if (box[0] == box[4] == box[8] != " "):
    return True
  if (box[2] == box[4] == box[6] != " "):
    return True
  return False

def answer(count) -> None:
  if (count > 0):
    print(("1st" if (count % 2) else "2nd") + " won")
  else:
    print("Friendship won")

def replay(func):
  def wrapper():
    func()
    if (input("Again? (y/n) ") == "y"):
      main()
  return wrapper

@replay
def main() -> None:
  print("\nTic-tac-toe game:")
  count, box = 9, [" "]*9
  cout(box)
  while (count):
    if (reading(box, count)):
      cout(box)
      if (check(box)):
        break
      count -= 1
  answer(count)

main()
